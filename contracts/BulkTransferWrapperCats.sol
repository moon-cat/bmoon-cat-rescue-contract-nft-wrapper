// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;

interface MoonCatsWrapped {
    function tokenByIndex(uint index) external view returns(uint256);
    function ownerOf(uint tokenId) external view returns(address);
    function totalSupply() external view returns (uint256);
    function _tokenIDToCatID(uint) external view returns(bytes5);
    function _catIDToTokenID(bytes5) external view returns(uint);
    function transferFrom(address from, address to, uint256 tokenId) external;
    function isApprovedForAll(address owner, address operator) external view returns (bool);
}

contract BulkTransferWrapperCats {
    MoonCatsWrapped public _moonCatsWrapper = MoonCatsWrapped(0x7CfAa5ca534afcd3a98509c9b8652ad0C1C2B3d8); // mainnet
//    MoonCatsWrapped public _moonCatsWrapper = MoonCatsWrapped(0x7ce6CF36cA71D6D6dF761D30064407da378627c3); // testnet

    function transferAllWrappedCats(address to) public {
        require(_moonCatsWrapper.isApprovedForAll(msg.sender, address(this)), 'Please call setApprovalForAll method first');
        for (uint i = 0; i < _moonCatsWrapper.totalSupply(); i++) {
            uint tokenId = _moonCatsWrapper.tokenByIndex(i);
            if (_moonCatsWrapper.ownerOf(tokenId) == msg.sender) {
                _moonCatsWrapper.transferFrom(msg.sender, to, tokenId);
            }
        }
    }

    function transferSomeWrappedCatsByCatId(bytes5[] memory catIds, address to) public {
        for (uint i = 0; i < catIds.length; i++) {
            uint tokenId = _moonCatsWrapper._catIDToTokenID(catIds[i]);
            if (_moonCatsWrapper.ownerOf(tokenId) == msg.sender) {
                _moonCatsWrapper.transferFrom(msg.sender, to, tokenId);
            }
        }
    }

    function transferSomeWrappedCatsByTokenId(uint[] memory tokenIds, address to) public {
        for (uint i = 0; i < tokenIds.length; i++) {
            uint tokenId = tokenIds[i];
            if (_moonCatsWrapper.ownerOf(tokenId) == msg.sender) {
                _moonCatsWrapper.transferFrom(msg.sender, to, tokenId);
            }
        }
    }
}
