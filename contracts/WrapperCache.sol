// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

interface MoonCatsWrapped {

    function tokenByIndex(uint index) external view returns(uint256);
    function ownerOf(uint tokenId) external view returns(address);
    function totalSupply() external view returns (uint256);
    function _tokenIDToCatID(uint) external view returns(bytes5);
}

contract WrapperCache {
    MoonCatsWrapped public _moonCatsWrapper = MoonCatsWrapped(0x7CfAa5ca534afcd3a98509c9b8652ad0C1C2B3d8);

    struct CatToken {
        uint tokenId;
        bytes5 catId;
        address owner;
    }

    function getAllTokens() public view returns(CatToken[] memory) {
        uint totalSupply = _moonCatsWrapper.totalSupply();
        CatToken[] memory cats = new CatToken[](totalSupply);

        for (uint i = 0; i < totalSupply; i++) {
            uint tokenId = _moonCatsWrapper.tokenByIndex(i);
            cats[i] = CatToken(tokenId, _moonCatsWrapper._tokenIDToCatID(tokenId), _moonCatsWrapper.ownerOf(tokenId));
        }

        return cats;
    }
}
